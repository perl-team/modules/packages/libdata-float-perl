Source: libdata-float-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ivan Kohler <ivan-debian@420.am>,
           Angel Abad <angel@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libtest-pod-coverage-perl,
                     libtest-pod-perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdata-float-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdata-float-perl.git
Homepage: https://metacpan.org/release/Data-Float

Package: libdata-float-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: Perl module encapsulating the floating point data type
 Data::Float is about the native floating point numerical data type.
 A floating point number is one of the types of datum that can appear
 in the numeric part of a Perl scalar.  This module supplies constants
 describing the native floating point type, classification functions,
 and functions to manipulate floating point values at a low level.
